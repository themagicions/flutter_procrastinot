import 'package:flutter/material.dart';
import 'package:flutter_procrastinot/constants.dart';
import 'package:flutter_procrastinot/ui/views/guestpage.dart';
import 'package:flutter_procrastinot/ui/views/login.dart';
import 'package:flutter_procrastinot/ui/views/signin.dart';
import 'package:flutter_procrastinot/ui/views/home.dart';
import 'package:flutter_procrastinot/ui/views/personal.dart';
import 'package:flutter_procrastinot/ui/views/professional.dart';
import 'package:flutter_procrastinot/ui/projects/addnotes.dart';



class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case start:
        return MaterialPageRoute(builder: (_) => LoginPage());
      case signin:
        return MaterialPageRoute(builder: (_) => SigninPage());
      case home:
        return MaterialPageRoute(builder: (_) => HomePage());
      case personal:
        return MaterialPageRoute(builder: (_) => PersonalPage());
      case professional:
        return MaterialPageRoute(builder: (_) => ProfessionalPage());
      case addnotes:
        return MaterialPageRoute(builder: (_) => AddNotesPage());
      case guest:
        return MaterialPageRoute(builder: (_) => GuestPage());
      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
              body: Center(
                  child: Text('No route defined for ${settings.name}')),
            ));
    }
  }
}