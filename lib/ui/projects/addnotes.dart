import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_procrastinot/core/services/offlineServices/ListsServices.dart';




class AddNotesPage extends StatefulWidget {

  @override
  AddNotesApp createState() {
    return AddNotesApp();
  }
}

class AddNotesApp extends State<AddNotesPage>{
  bool _checkedValue = false;
  String _textString = "sample";
  TextEditingController titleController = TextEditingController();
  TextEditingController contentController = TextEditingController();
  TextEditingController TodoController = TextEditingController();

  FocusNode titleFocus = FocusNode();
  FocusNode contentFocus = FocusNode();
  FocusNode TodoFocus = FocusNode();

  List<String> _list = ['charisse','charise','charice','charizz','charissy'];
  bool _isChecked = true;

  _onPressed(text) {
    int count = _list.length;
    setState(() {
      _list.add(text);
    });
  }

  bool Val = false;



  TextEditingController _textEditingController;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    //Map<>
    return Scaffold(
      appBar: AppBar(//title: Text('Create New Note'),
      ),
      body: Stack(
          children:<Widget>[
            ListView(

            )
          ]
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.white70,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.check),
            title: Text('Complete'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.cancel),
            title: Text('Cancel'),
          ),
        ],
      ),
        floatingActionButton: FloatingActionButton(
            child: Icon(Icons.add),
          onPressed: () async {
            return AlertDialog(
              title: Text('To-do Lists', style: TextStyle(fontSize: 15.0)),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new TextFormField(
                    autocorrect: false,
                    controller: _textEditingController,
                    decoration: InputDecoration(hintText: "Set name..."),
                  ),
                 // checkbox("Mon", Val),
                  SizedBox(
                    height: 20.0,
                  ),
                ],
              ),
              actions: <Widget>[
                new FlatButton(onPressed: () {
                  //_textEditingController.clear();
                  Navigator.of(context).pop();
                },
                  child: new Text('CANCEL'),
                  textColor: Colors.deepPurpleAccent[100],
                ),
                new RaisedButton(
                  color: Colors.deepPurpleAccent[100],
                  textColor: Colors.white,
                  onPressed: () async {
                    Navigator.of(context).pop();
                  },
                  child: new Text('DONE'),
                ),
              ],
            );
        },
        ),

    );
  }

  Widget checkbox(String title, bool boolValue) {

    return CheckboxListTile(
        title: TextField(decoration: InputDecoration(
    border: InputBorder.none,
    hintText: 'Enter to-do here',
    ),
          onSubmitted: (title) {
          checkbox(title, boolValue);
          },
        ),
        value: boolValue,
        controlAffinity: ListTileControlAffinity.leading,
        onChanged: (bool value) {
          setState(() {
            Val = value;
          });
        },
      );
  }


  void _doSomething(String text) {
    setState(() {
      //_checkedValue = value;
      _textString=text;
    });
  }
}