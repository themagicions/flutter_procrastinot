import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';




class AddTo_DoListApp extends StatefulWidget {

  @override
  AddTo_DoListAppState createState() {
    return AddTo_DoListAppState();
  }
}

class AddTo_DoListAppState extends State<AddTo_DoListApp>{
  bool _checkedValue = false;
  String _textString = "sample";
  TextEditingController titleController = TextEditingController();
  TextEditingController contentController = TextEditingController();
  TextEditingController TodoController = TextEditingController();

  FocusNode titleFocus = FocusNode();
  FocusNode contentFocus = FocusNode();
  FocusNode TodoFocus = FocusNode();

  List<String> _list = ['charisse','charise','charice','charizz','charissy'];
  bool _isChecked = true;

  _onPressed(text) {
    int count = _list.length;
    setState(() {
      _list.add(text);
    });
  }

  bool Val = false;



  TextEditingController _textEditingController;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    //Map<>
    return Scaffold(
      appBar: AppBar(//title: Text('Create New Note'),
      ),
      body: Stack(
          children:<Widget>[

            ListView(
              children: <Widget>[
                Container(
                  height:10,
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: TextField(
                    focusNode: titleFocus,
                    autofocus: true,
                    controller: titleController,
                    keyboardType: TextInputType.multiline,
                    maxLines: null,
                    onSubmitted: (text) {
                      titleFocus.unfocus();
                      FocusScope.of(context).requestFocus(contentFocus);
                    },
                    onChanged: (value) {
                    },
                    textInputAction: TextInputAction.next,
                    style: TextStyle(
                        fontFamily: 'ZillaSlab',
                        fontSize: 32,
                        fontWeight: FontWeight.w700),
                    decoration: InputDecoration.collapsed(
                      hintText: 'Enter a title',
                      hintStyle: TextStyle(
                          color: Colors.grey.shade400,
                          fontSize: 32,
                          fontFamily: 'ZillaSlab',
                          fontWeight: FontWeight.w700),
                      border: InputBorder.none,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: TextField(
                    focusNode: contentFocus,
                    controller: contentController,
                    keyboardType: TextInputType.multiline,
                    maxLines: null,

                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                    decoration: InputDecoration.collapsed(
                      hintText: 'Start typing...',
                      hintStyle: TextStyle(
                          color: Colors.grey.shade400,
                          fontSize: 18,
                          fontWeight: FontWeight.w500),
                      border: InputBorder.none,
                    ),
                  ),
                ),

              ],
            )
          ]
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.white70,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.check),
            title: Text('Complete'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.cancel),
            title: Text('Cancel'),
          ),
        ],
      ),

    );
  }

}