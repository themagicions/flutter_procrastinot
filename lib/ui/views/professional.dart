import 'dart:async';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_procrastinot/authentication.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:flutter_procrastinot/core/services/onlineServices/FirebaseDatabase.dart';
import 'package:grouped_buttons/grouped_buttons.dart';
import 'package:intl/intl.dart';
import 'package:flutter_procrastinot/local_ notifications_helper.dart';

import 'home.dart';

TextEditingController _codeEditingController = new TextEditingController(text: "12345678");

EditCodeFormState(String projectName) {
  return _codeEditingController = TextEditingController(text: projectName);
}

class AddProjectDialog extends StatefulWidget {

  @override
  AddProjectDialogState createState(){
    return AddProjectDialogState();
  }
}
class AddProjectDialogState extends State<AddProjectDialog> {
  bool Val = false;
  final String payload;
  List<String> _list = [];
  List<bool> _status = [];
  String _date = "";
  String _time = "";
  DateTime _dateTime;
  int id = 100;

  TextEditingController titleController = TextEditingController();
  TextEditingController noteController = TextEditingController();
  TextEditingController contentController = TextEditingController();
  final TextEditingController TodoController = new TextEditingController();

  TextEditingController check1 = TextEditingController();

  FocusNode titleFocus = FocusNode();
  FocusNode noteFocus = FocusNode();
  FocusNode contentFocus = FocusNode();
  FocusNode TodoFocus = FocusNode();
  final notifications = FlutterLocalNotificationsPlugin();

  AddProjectDialogState({
    @required this.payload,
    Key key,
  }) : super();

  @override
  void initState() {
    super.initState();

    final settingsAndroid = AndroidInitializationSettings('logo');
    final settingsIOS = IOSInitializationSettings(
        onDidReceiveLocalNotification: (id, title, body, payload) =>
            onSelectNotification(payload));

    notifications.initialize(
        InitializationSettings(settingsAndroid, settingsIOS),
        onSelectNotification: onSelectNotification);
  }

  Future onSelectNotification(String payload) async => await Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => HomePage(payload: payload)),
  );

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: GradientAppBar(
        gradient: LinearGradient(colors: [Colors.pinkAccent[100], Colors.deepPurpleAccent[200]]),
        //title: Text('Create New Note'),
      ),
      body: Stack(
          children: <Widget>[
            Column(
              children: <Widget>[
                TextField(
                  focusNode: titleFocus,
                  autofocus: true,
                  controller: titleController,
                  keyboardType: TextInputType.multiline,
                  maxLines: null,
                  onSubmitted: (text) {
                    titleFocus.unfocus();
                    FocusScope.of(context).requestFocus(contentFocus);
                  },
                  onChanged: (value) {},
                  textInputAction: TextInputAction.next,
                  style: TextStyle(
                      fontFamily: 'ZillaSlab',
                      fontSize: 32,
                      fontWeight: FontWeight.w700),
                  decoration: InputDecoration.collapsed(
                    hintText: 'Enter a title',
                    hintStyle: TextStyle(
                        color: Colors.grey.shade400,
                        fontSize: 28,
                        fontFamily: 'ZillaSlab',
                        fontWeight: FontWeight.w700),
                    border: InputBorder.none,
                  ),
                ),
                 SizedBox(height: 10),
                Text("Set Alarm",
                    style: TextStyle(fontFamily: "VT323", fontSize: 18.0, fontWeight: FontWeight.bold,
                        color: Colors.pinkAccent[100])),
                SizedBox(height: 10),
                FlatButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0)),
                  onPressed: () {
                    DatePicker.showDatePicker(context,
                        theme: DatePickerTheme(
                          containerHeight: 210.0,
                        ),
                        showTitleActions: true,
                        minTime: DateTime.now(),
                        maxTime: DateTime(2022, 12, 31), onConfirm: (date) {
                          print('confirm $date');
                          var mon = '${date.month}';
                          var d = '${date.day}';

                          if(int.parse(mon) < 10) mon = '0' + mon;
                          if(int.parse(d) < 10) d = '0' + d;

                          _date = '${date.year}-' + mon + '-' + d;
                          setState(() {});
                        }, currentTime: DateTime.now(), locale: LocaleType.en);
                  },
                  child: Container(
                    alignment: Alignment.center,
                    height: 50.0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Container(
                              child: Row(
                                children: <Widget>[
                                  Icon(
                                    Icons.date_range,
                                    size: 18.0,
                                    color: Colors.pinkAccent[100],
                                  ),
                                  Text(
                                    " $_date",
                                    style: TextStyle(
                                        color: Colors.pinkAccent[100],
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18.0),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                        Text(
                          "  Change",
                          style: TextStyle(
                              color: Colors.deepPurpleAccent[100],
                              fontWeight: FontWeight.bold,
                              fontSize: 18.0),
                        ),
                      ],
                    ),
                  ),
                  color: Colors.white,
                ),
                FlatButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0)),
                  onPressed: () {
                    DatePicker.showTimePicker(context,
                        theme: DatePickerTheme(
                          containerHeight: 210.0,
                        ),
                        showTitleActions: true, onConfirm: (time) {
                          print('confirm $time');
                          var hour = '${time.hour}';
                          var min = '${time.minute}';

                          if(int.parse(hour) < 10) hour = '0' + hour;
                          if(int.parse(min) < 10) min = '0' + min;

                          _time = hour + ':' + min;
                          setState(() {});
                        }, currentTime: DateTime.now(), locale: LocaleType.en);
                    setState(() {});
                  },
                  child: Container(
                    alignment: Alignment.center,
                    height: 50.0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Container(
                              child: Row(
                                children: <Widget>[
                                  Icon(
                                    Icons.access_time,
                                    size: 18.0,
                                    color: Colors.pinkAccent[100],
                                  ),
                                  Text(
                                    " $_time",
                                    style: TextStyle(
                                        color: Colors.pinkAccent[100],
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18.0),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                        Text(
                          "  Change",
                          style: TextStyle(
                              color: Colors.deepPurpleAccent[100],
                              fontWeight: FontWeight.bold,
                              fontSize: 18.0),
                        ),
                      ],
                    ),
                  ),
                  color: Colors.white,
                ),
                SizedBox(height: 10),
                Container(
                    color: Colors.deepPurpleAccent[100],
                    padding: EdgeInsets.all(3.0),
                    child: TextField(
                      focusNode: noteFocus,
                      autofocus: true,
                      controller: noteController,
                      keyboardType: TextInputType.multiline,
                      maxLines: null,
                      onSubmitted: (text) {
                        noteFocus.unfocus();
                        FocusScope.of(context).requestFocus(contentFocus);
                      },
                      onChanged: (value) {},
                      textInputAction: TextInputAction.next,
                      style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'ZillaSlab',
                          fontSize: 20,
                          fontWeight: FontWeight.w700),
                      decoration: InputDecoration.collapsed(
                        //filled: true,
                        //fillColor: Colors.deepPurpleAccent[100],
                        hintText: 'Notes Here',
                        hintStyle: TextStyle(
                            color: Colors.white30,
                            fontSize: 20,
                            fontFamily: 'ZillaSlab',
                            fontWeight: FontWeight.w700),
                        border: InputBorder.none,
                      ),
                    ),
                  ),
                //SizedBox(height: 20),
                Container(
                  color: Colors.pinkAccent[100],
                  padding: EdgeInsets.all(3.0),
                  child: TextField(
                    decoration: InputDecoration.collapsed(
                      hintText: 'Enter to do list',
                      hintStyle: TextStyle(
                          color: Colors.white30,
                          fontSize: 20,
                          fontWeight: FontWeight.w700),
                      border: InputBorder.none,
                    ),
                    controller: TodoController,
                    //autofocus: true,
                    onSubmitted: (text) {
                      _list.add(text);
                      TodoController.clear();
                      setState(() {});
                    },
                    style: TextStyle(color: Colors.white),
                  ),
                ),
                new Expanded(
                    child: new ListView.builder
                      (
                        itemCount: _list.length,
                        itemBuilder: (BuildContext context, int Index) {
                          return checkbox(_list[Index], false);
                        }
                    )
                ),
              ],
            ),
          ]
      ),
      bottomNavigationBar: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          RaisedButton(
            padding: const EdgeInsets.all(8.0),
            textColor: Colors.white,
            color: Colors.deepPurpleAccent[100],
            onPressed: () async {
              for(int x = 0; x < _list.length;x++)
              {
                _status.add(false);
              }
              _dateTime = DateTime.parse(_date + ' ' + _time);
              await showOngoingNotification(notifications, id: id,
                  title: "Reminder from ProcrastiNOT",
                  body: titleController.text, time: _dateTime);
              id++;
              //print(_codeEditingController.text);
              CRUDProject.createFirebaseDatabase(_codeEditingController.text, titleController.text, Project(titleController.text,noteController.text,_date + ' ' + _time,_status,_list,
                  DateFormat('kk:mm:ss EEE MMM d y').format(DateTime.now()),getCurrentEmail()));
              TodoController.clear();
              Navigator.of(context).pop();
            },
            child: new Text("Save"),
            elevation: 4.0,
          ),
          SizedBox(width: 100.0),
          RaisedButton(
            padding: const EdgeInsets.all(8.0),
            textColor: Colors.purpleAccent[100],
            color: Colors.white,
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: new Text("Cancel"),
            elevation: 4.0,
          ),
        ],
      ),
    );
  }
}

Widget checkbox(String title, bool boolValue) {
  return CheckboxListTile(
    title: Text(title), //    <-- label
    value: boolValue,
    onChanged: (newValue){
    },
  );
}

class ProfessionalPage extends StatefulWidget {

  @override
  ProfessionalPageState createState(){
    return ProfessionalPageState();
  }
}

class ProfessionalPageState extends State<ProfessionalPage> {

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: GradientAppBar(
            title: Text('Professional Page'),
            bottomOpacity: 0.8,
            gradient: LinearGradient(
                colors: [Colors.pinkAccent[100], Colors.deepPurpleAccent[200]]),
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.expand_more),
                color: Colors.white,
                onPressed: () {
                  return showDialog(context: context,
                      builder: (context) {
                        return AlertDialog(
                          content: SingleChildScrollView(
                            child: TextField(
                              controller: _codeEditingController,
                              decoration: InputDecoration(
                                hintText: "Enter code...",
                              ),
                            ),
                          ),
                          actions: <Widget>[
                            new FlatButton(onPressed: () {
                              Navigator.of(context).pop();
                            },
                              child: new Text('CANCEL'),
                              textColor: Colors.deepPurpleAccent[100],
                            ),
                            new RaisedButton(
                              color: Colors.deepPurpleAccent[100],
                              textColor: Colors.white,
                              onPressed: () async {
                                EditCodeFormState(_codeEditingController.text);
                                await Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (BuildContext context) => ProfessionalPage(),
                                      fullscreenDialog: true,
                                    )
                                );
                                setState(() {Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (BuildContext context) => HomePage(),
                                      fullscreenDialog: true,
                                    )
                                );});
                              },
                              child: new Text('DONE'),
                            ),
                          ],
                        );
                      });
                },
              ),
            ],
            bottom: TabBar(tabs: <Widget>[
              Tab(
                icon: Icon(Icons.group_work),
                text: 'Projects',
                ),
              Tab(
                icon: Icon(Icons.group_work),
                text: 'NewTab',
              )

            ]),
          ),
          body: Container(
              child: TabBarView(
            children: <Widget>[
                Projects(),
                Projects(),
            ],
          )),
        ));
  }
}


  class Projects extends StatefulWidget {
  @override
  ProjectsApp createState() {
    return ProjectsApp();
  }
}

class ProjectsApp extends State<Projects> {
  List<Project> items;
  Query _query;
  bool Val = false;
  List<String> _list = [];
  List<String> _list1 = [];
  List<bool> _status = [];
  String _date = "";
  String _time = "";
  DateTime _dateTime;

  TextEditingController _EditDateController = TextEditingController();
  TextEditingController _EdittitleController = TextEditingController();
  TextEditingController _EditnoteController = TextEditingController();
  TextEditingController _EditcontentController = TextEditingController();
  TextEditingController _EdittimeController = TextEditingController();
  final TextEditingController _EditTodoController = new TextEditingController();

  EditTitleController(String x)
  {
   return _EdittitleController = TextEditingController(text: x);
  }
  EditNoteController(String x)
  {
    return _EditnoteController = TextEditingController(text: x);
  }
  TextEditingController check1 = TextEditingController();

  FocusNode titleFocus = FocusNode();
  FocusNode noteFocus = FocusNode();
  FocusNode contentFocus = FocusNode();
  FocusNode TodoFocus = FocusNode();

  final String payload;

  final notifications = FlutterLocalNotificationsPlugin();

  ProjectsApp({
    @required this.payload,
    Key key,
  }) : super();

  Future onSelectNotification(String payload) async => await Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => HomePage(payload: payload)),
  );



  @override
  void initState() {
    CRUDProject.queryProjects(_codeEditingController.text).then((Query query) {
      setState(() {
        _query = query;
      });
    });
    print(_query);

    final settingsAndroid = AndroidInitializationSettings('logo');
    final settingsIOS = IOSInitializationSettings(
        onDidReceiveLocalNotification: (id, title, body, payload) =>
            onSelectNotification(payload));

    notifications.initialize(
        InitializationSettings(settingsAndroid, settingsIOS),
        onSelectNotification: onSelectNotification);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

    @override
    Widget build(BuildContext context) {
      Widget body = new ListView(
        children: <Widget>[
          new ListTile(
            title: new Text("The list is empty...", style: TextStyle(color: Colors.white),),
          )
        ],
      );

      if (_query != null) {
        body = new FirebaseAnimatedList(
          query: _query,
          itemBuilder: (
              BuildContext context,
              DataSnapshot snapshot,
              Animation<double> animation,
              int index,
              ) {
            Map map = snapshot.value;
            String projectname = map['project name'] as String;
            String name = map['person modified'] as String;
            String date = map['date modified'] as String;
            String notes= map['notes'] as String;
            String reminder = map['reminder'] as String;
            List<String> list = map['list']?.cast<String>();
            List<bool> status = map['status']?.cast<bool>();
            return new Column(
              children: <Widget>[
                Card(
                  color: Colors.white70,
                  child: Dismissible(
                      key: UniqueKey(),
                      background: Container(color: Colors.red),
                      onDismissed: (direction) {
                        deleteFirebaseDatabase(_codeEditingController.text, projectname);
                        Scaffold
                            .of(context)
                            .showSnackBar(SnackBar(content: Text("Project dismissed")));
                      },
                      child: new ListTile(
                        title: Text(projectname),
                        subtitle: Text(name +" ( "+ date +" )"),
                        leading: Icon(Icons.group_work),
                        trailing: IconButton(icon: Icon(Icons.edit),),
                          onTap:(){
                          _list = new List<String>();
                          _status = new List<bool>();
                            int length = (list.length == null)?0:list.length;
                            for(int x = 0;x<length;x++)
                            {
                              _list.add(list.elementAt(x).toString());
                              _status.add(status.elementAt(x));
//                              _list1.add((_status.elementAt(x) == false)?"gg":_list.elementAt(x));
                            }
                             return showDialog(context: context,
                                 builder: (context) {
                                   return Scaffold(
                                     appBar: GradientAppBar(
                                       gradient: LinearGradient(colors: [Colors.pinkAccent[100], Colors.deepPurpleAccent[200]]),
                                       //title: Text('Create New Note'),
                                     ),
                                     body: Stack(
                                         children: <Widget>[
                                           Column(
                                             children: <Widget>[
                                               TextField(
                                                 focusNode: titleFocus,
                                                 autofocus: true,
                                                 readOnly: true,
                                                 controller: EditTitleController(projectname),
                                                 keyboardType: TextInputType.multiline,
                                                 maxLines: null,
                                                 onSubmitted: (text) {
                                                   titleFocus.unfocus();
                                                   FocusScope.of(context).requestFocus(contentFocus);
                                                 },
                                                 onChanged: (value) {},
                                                 textInputAction: TextInputAction.next,
                                                 style: TextStyle(
                                                     fontFamily: 'ZillaSlab',
                                                     fontSize: 32,
                                                     fontWeight: FontWeight.w700),
                                                 decoration: InputDecoration.collapsed(
                                                   hintText: 'Enter a title',
                                                   hintStyle: TextStyle(
                                                       color: Colors.grey.shade400,
                                                       fontSize: 28,
                                                       fontFamily: 'ZillaSlab',
                                                       fontWeight: FontWeight.w700),
                                                   border: InputBorder.none,
                                                 ),
                                               ),
                                               SizedBox(height: 10),
                                               Text("Set Alarm",
                                                   style: TextStyle(fontFamily: "VT323", fontSize: 18.0, fontWeight: FontWeight.bold,
                                                       color: Colors.pinkAccent[100])),
                                               SizedBox(height: 10),
                                               FlatButton(
                                                 shape: RoundedRectangleBorder(
                                                     borderRadius: BorderRadius.circular(5.0)),
                                                 onPressed: () {
                                                   DatePicker.showDatePicker(context,
                                                       theme: DatePickerTheme(
                                                         containerHeight: 210.0,
                                                       ),
                                                       showTitleActions: true,
                                                       minTime: DateTime.now(),
                                                       maxTime: DateTime(2022, 12, 31), onConfirm: (date) {
                                                         print('confirm $date');
                                                         var mon = '${date.month}';
                                                         var d = '${date.day}';

                                                         if(int.parse(mon) < 10) mon = '0' + mon;
                                                         if(int.parse(d) < 10) d = '0' + d;

                                                         _date = '${date.year}-' + mon + '-' + d;
                                                         setState(() {});
                                                       }, currentTime: DateTime.now(), locale: LocaleType.en);
                                                 },
                                                 child: Container(
                                                   alignment: Alignment.center,
                                                   height: 50.0,
                                                   child: Row(
                                                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                     children: <Widget>[
                                                       Row(
                                                         children: <Widget>[
                                                           Container(
                                                             child: Row(
                                                               children: <Widget>[
                                                                 Icon(
                                                                   Icons.date_range,
                                                                   size: 18.0,
                                                                   color: Colors.pinkAccent[100],
                                                                 ),
                                                                 Text(
                                                                   reminder.split(" ").elementAt(0),
                                                                   style: TextStyle(
                                                                       color: Colors.pinkAccent[100],
                                                                       fontWeight: FontWeight.bold,
                                                                       fontSize: 18.0),
                                                                 ),
                                                               ],
                                                             ),
                                                           )
                                                         ],
                                                       ),
                                                       Text(
                                                         "  Change",
                                                         style: TextStyle(
                                                             color: Colors.deepPurpleAccent[100],
                                                             fontWeight: FontWeight.bold,
                                                             fontSize: 18.0),
                                                       ),
                                                     ],
                                                   ),
                                                 ),
                                                 color: Colors.white,
                                               ),
                                               FlatButton(
                                                 shape: RoundedRectangleBorder(
                                                     borderRadius: BorderRadius.circular(5.0)),
                                                 onPressed: () {
                                                   DatePicker.showTimePicker(context,
                                                       theme: DatePickerTheme(
                                                         containerHeight: 210.0,
                                                       ),
                                                       showTitleActions: true, onConfirm: (time) {
                                                         print('confirm $time');
                                                         var hour = '${time.hour}';
                                                         var min = '${time.minute}';

                                                         if(int.parse(hour) < 10) hour = '0' + hour;
                                                         if(int.parse(min) < 10) min = '0' + min;

                                                         _time = hour + ':' + min;
                                                         setState(() {});
                                                       }, currentTime: DateTime.now(), locale: LocaleType.en);
                                                   setState(() {});
                                                 },
                                                 child: Container(
                                                   alignment: Alignment.center,
                                                   height: 50.0,
                                                   child: Row(
                                                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                     children: <Widget>[
                                                       Row(
                                                         children: <Widget>[
                                                           Container(
                                                             child: Row(
                                                               children: <Widget>[
                                                                 Icon(
                                                                   Icons.access_time,
                                                                   size: 18.0,
                                                                   color: Colors.pinkAccent[100],
                                                                 ),
                                                                 Text(
                                                                     reminder.split(" ").elementAt(1),
                                                                   style: TextStyle(
                                                                       color: Colors.pinkAccent[100],
                                                                       fontWeight: FontWeight.bold,
                                                                       fontSize: 18.0),
                                                                 ),
                                                               ],
                                                             ),
                                                           )
                                                         ],
                                                       ),
                                                       Text(
                                                         "  Change",
                                                         style: TextStyle(
                                                             color: Colors.deepPurpleAccent[100],
                                                             fontWeight: FontWeight.bold,
                                                             fontSize: 18.0),
                                                       ),
                                                     ],
                                                   ),
                                                 ),
                                                 color: Colors.white,
                                               ),
                                               SizedBox(height: 10),
                                               Container(
                                                 color: Colors.deepPurpleAccent[100],
                                                 padding: EdgeInsets.all(3.0),
                                                 child: TextField(
                                                   focusNode: noteFocus,
                                                   autofocus: true,
                                                   controller: EditTitleController(notes),
                                                   keyboardType: TextInputType.multiline,
                                                   maxLines: null,
                                                   onSubmitted: (text) {
                                                     noteFocus.unfocus();
                                                     FocusScope.of(context).requestFocus(contentFocus);
                                                   },
                                                   onChanged: (value) {},
                                                   textInputAction: TextInputAction.next,
                                                   style: TextStyle(
                                                       color: Colors.white,
                                                       fontFamily: 'ZillaSlab',
                                                       fontSize: 20,
                                                       fontWeight: FontWeight.w700),
                                                   decoration: InputDecoration.collapsed(
                                                     //filled: true,
                                                     //fillColor: Colors.deepPurpleAccent[100],
                                                     hintText: 'Notes Here',
                                                     hintStyle: TextStyle(
                                                         color: Colors.white30,
                                                         fontSize: 20,
                                                         fontFamily: 'ZillaSlab',
                                                         fontWeight: FontWeight.w700),
                                                     border: InputBorder.none,
                                                   ),
                                                 ),
                                               ),
                                               //SizedBox(height: 20),
                                               Container(
                                                 color: Colors.pinkAccent[100],
                                                 padding: EdgeInsets.all(3.0),
                                                 child: TextField(
                                                   decoration: InputDecoration.collapsed(
                                                     hintText: 'Enter to do list',
                                                     hintStyle: TextStyle(
                                                         color: Colors.white30,
                                                         fontSize: 20,
                                                         fontWeight: FontWeight.w700),
                                                     border: InputBorder.none,
                                                   ),
                                                   controller: _EditTodoController,
                                                   autofocus: true,
                                                   onSubmitted: (text) {
                                                     _list.add(text);
                                                     _status.add(false);
                                                     _EditTodoController.clear();
                                                   },
                                                   style: TextStyle(color: Colors.white),
                                                 ),
                                               ),
                                             new Expanded(

                                               child: SingleChildScrollView(
                                                 child: CheckboxGroup(
                                                   labels: _list,

                                                   onChange: (bool isChecked, String label, int index) =>
                                                   {
                                                     _status.removeAt(index),
                                                     _status.insert(index, isChecked),
                                                   },
                                                 ),
                                               ),
                                             ),
                                             ],
                                           ),
                                         ]
                                     ),
                                     bottomNavigationBar: Row(
                                       mainAxisAlignment: MainAxisAlignment.center,
                                       mainAxisSize: MainAxisSize.max,
                                       children: <Widget>[
                                         RaisedButton(
                                           padding: const EdgeInsets.all(8.0),
                                           textColor: Colors.white,
                                           color: Colors.deepPurpleAccent[100],
                                           onPressed: () async {

                                             CRUDProject.updateFirebaseDatabase(_codeEditingController.text, projectname,
                                             Project(projectname,_EditnoteController.text,((_date=="")?reminder.split(" ").elementAt(0):_date) + ' ' + ((_time=="")?reminder.split(" ").elementAt(1):_time),
                                                 _status,_list,
                                             DateFormat('kk:mm:ss EEE MMM d y').format(DateTime.now()),getCurrentEmail()));
                                             Navigator.of(context).pop();
                                           },
                                           child: new Text("Save"),
                                           elevation: 4.0,
                                         ),
                                         SizedBox(width: 100.0),
                                         RaisedButton(
                                           padding: const EdgeInsets.all(8.0),
                                           textColor: Colors.purpleAccent[100],
                                           color: Colors.white,
                                           onPressed: () {
                                             Navigator.of(context).pop();                                           },
                                           child: new Text("Cancel"),
                                           elevation: 4.0,
                                         ),
                                       ],
                                     ),
                                   );

                                 });
                          }
                  )
                ),

                ),
              ],
            );
          },
        );
      }

      return Scaffold(
        body: Stack(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: ExactAssetImage('assets/bg3.jpg'),
                  fit: BoxFit.cover,
                ),
              ),
            ),

            body,
          ],
        ),

        floatingActionButton: FloatingActionButton.extended(
          heroTag: 'project',
          onPressed: () async {
            return showDialog(context: context,
                builder: (context) {
                  return AddProjectDialog();
                });
          },
          icon: Icon(Icons.add_to_queue),
          label: Text('Add Project'),
          backgroundColor: Colors.transparent,
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      );
    }
  }


void addFirebaseDatabase(
    String password,
    String projectTitle,
    Project project)
{
  CRUDProject.createFirebaseDatabase(password, projectTitle, project);
}

void updateFirebaseDatabase(
    String password,
    String projectTitle,
    Project project)
{
  CRUDProject.updateFirebaseDatabase(
      password, projectTitle,project);
}

void deleteFirebaseDatabase(String password, String projectTitle) {
  CRUDProject.deleteFirebaseDatabase(password, projectTitle);
}
