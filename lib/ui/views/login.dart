import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_procrastinot/authentication.dart';
import 'package:flutter_procrastinot/constants.dart';

class LoginPage extends StatefulWidget{
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage>{


  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: Stack(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: ExactAssetImage('assets/giphy2.gif'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            ListView(
              padding: EdgeInsets.fromLTRB(100.0, 0.0, 100.0, 200.0),
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                      image: DecorationImage(
                        image: ExactAssetImage('assets/logo.png'),
                        fit: BoxFit.cover,
                      )
                  ),
                ),
                SizedBox(height: 200.0),
                Column(
                  children: <Widget>[
                    Image.asset('assets/logo.png',
                        width: 80.0,
                        height: 80.0),
                    SizedBox(height: 16.0),
                    Text('ProcrastiNOT', style: TextStyle(color: Colors.white, fontFamily: "VT323", fontSize: 24.0)),
                  ],
                ),
                const SizedBox(height: 50.0),
                CupertinoButton(
                  onPressed: () {
                    if(isSignedIn() == false)
                    {
                      Navigator.pushNamed(context, signin);
                    }
                    else
                      {
                        Navigator.pushNamed(context, home);
                      }
                  },
                  color: Colors.deepPurple,
                  child: Text('Get Started'),
                  padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                  pressedOpacity: 0.7,
                ),
              ],
            ),
          ]
      ),
    );
  }
}