import 'package:flutter/material.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:flutter_procrastinot/constants.dart';
import 'package:flutter_procrastinot/authentication.dart';


class SigninPage extends StatefulWidget{
  @override
  _SigninPageState createState() => _SigninPageState();
}

class _SigninPageState extends State<SigninPage>{
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
      return Scaffold(
        body: Stack(
          children: <Widget> [
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: ExactAssetImage('assets/giphy2.gif'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Center(
              child: _isLoading? CircularProgressIndicator():
              ListView(
                  padding: EdgeInsets.fromLTRB(40.0, 0.0, 40.0, 100.0),
                  children: <Widget>[
                    SizedBox(height: 330.0),
                    Container(
                      child: SignInButton(
                        Buttons.Google,
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
                        text: "Sign in with Google",
                        onPressed: () async {
                          setState(() => _isLoading = true);
                          signInWithGoogle().whenComplete(() {
                            setState(() => _isLoading = false);
                            Navigator.pushNamed(context, home);
                          });
                          Scaffold.of(context).showSnackBar(
                                SnackBar(content: Text("Wrong email or password.")),
                          );
                          Navigator.popAndPushNamed(context, signin);
                        },
                      ),
                    ),
                    SizedBox(height: 5.0),
                    FlatButton(
                      onPressed: (){
                        Navigator.pushNamed(context, home);
                      },
                      splashColor: Colors.white,
                      child: Text(
                        'Skip >>',
                        style: TextStyle(color: Colors.white, fontSize: 15.0, fontFamily: "Comic Sans"),
                      ),
                    ),
                  ]
              ),
            ),

          ]
        ),
      );
  }
}