import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_procrastinot/constants.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:flutter_procrastinot/authentication.dart';


class HomePage extends StatelessWidget{
  final String payload;

  const HomePage({
    @required this.payload,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context){
    return Scaffold(
      backgroundColor: Colors.white,
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            SizedBox(height: 250.0),
            new Container (
              //color: Colors.deepPurpleAccent[100],
              child: new Column(
                children: <Widget> [
                  new Text(getCurrentEmail(), style: TextStyle(fontSize: 20.0,color: Colors.deepOrange),),
                  ListTile(
                    title: Text("Sign Out"),
                    leading: Icon(Icons.person_pin),
                    onTap: () {
                      signOutGoogle();
                      Navigator.pushNamed(context, signin);
                      AlertDialog(
                        title: Text('Signed out'),
                      );
                    },
                  ),
                  ListTile(
                    title: Text("About Team"),
                    leading: Icon(Icons.info),
                    onTap: () {
                      return showDialog(context: context,
                        builder: (context) {
                          return AlertDialog(
                            content: SingleChildScrollView(
                              child: Column(
                                children: <Widget>[
                                  Icon(Icons.group),
                                  SizedBox(height: 30.0),
                                  Text("Consists of four Computer Engineering students from"
                                      " Cebu Institute of Technology University."),
                                ],
                              ),
                            ),
                          );
                        });
                    },
                  ),
                ]
              ),
            ),
          ],
        ),
      ),
      appBar: GradientAppBar(
        gradient: LinearGradient(colors: [Colors.pinkAccent[100], Colors.deepPurpleAccent[200]]),
        bottomOpacity: 0.8,
        title: Text('Home'),
      ),
      body: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: ExactAssetImage('assets/giphy2.gif'),
                fit: BoxFit.cover,
              ),
            ),
          ),

          Center(
              child: ButtonBar(
                alignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(0.0),
                      width: 150,
                      height: 80,
                      child: new FloatingActionButton.extended(
                        backgroundColor: Colors.pinkAccent[200],
                        heroTag: 'btn1',
                        splashColor: Colors.pink,
                        elevation: 0,
                        icon: Icon(Icons.person),
                        label: Text('Personal'),
                        onPressed: () {
                          Navigator.pushNamed(context, personal);
                        },
                      ),
                    ),
                    Container(
                      width: 150,
                      height: 80,
                      child: new FloatingActionButton.extended(
                        backgroundColor: Colors.deepPurpleAccent,
                        heroTag: 'btn2',
                        splashColor: Colors.deepPurple,
                        elevation: 0,
                        icon: Icon(Icons.group),
                        label: Text('Professional'),
                        onPressed: () {
                          Navigator.pushNamed(context, professional);
                        },
                      ),
                    ),


                  ]
              )
          ),
          SizedBox(height: 500, width: 200),

          Center(
            child: Container(
              width: 100,
              height: 40,
              child: new FloatingActionButton.extended(
                backgroundColor: Colors.green,
                heroTag: 'btn3',
                splashColor: Colors.green,
                elevation: 0,
                icon: Icon(Icons.ac_unit),
                label: Text('Guest'),
                onPressed: () {
                  Navigator.pushNamed(context, guest);

                },
              ),
            ),

          ),
        ]
      ),
    );

  }
}