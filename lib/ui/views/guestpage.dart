import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';

class GuestPage extends StatefulWidget{
  @override
  GuestPageState createState(){
    return GuestPageState();
  }
}


class GuestPageState extends State<GuestPage>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: GradientAppBar(
        title: Text('Guest Page'),
        gradient: LinearGradient(colors: [Colors.pinkAccent[100], Colors.deepPurpleAccent[200]]),
      ),
    );

  }
  }

