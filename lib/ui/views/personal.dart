import 'dart:core' as prefix0;
import 'dart:core';
import 'dart:math';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_procrastinot/constants.dart';
import 'package:flutter_procrastinot/core/models/offlineModels/listitems.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_procrastinot/core/models/offlineModels/reminder.dart';
import 'package:flutter_procrastinot/core/services/offlineServices/NotesServices.dart';
import 'package:flutter_procrastinot/core/services/offlineServices/ReminderServices.dart';
import 'package:flutter_procrastinot/core/services/offlineServices/ListsServices.dart';
import 'package:flutter_procrastinot/ui/views/home.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:flutter_procrastinot/core/models/offlineModels/notes.dart';
import 'package:flutter_procrastinot/core/models/offlineModels/lists.dart';
import 'package:grouped_buttons/grouped_buttons.dart';
import 'package:intl/intl.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter_procrastinot/local_ notifications_helper.dart';

class PersonalPage extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: GradientAppBar(
            gradient: LinearGradient(colors: [Colors.pinkAccent[100], Colors.deepPurpleAccent[200]]),
            bottomOpacity: 0.8,
            title: Text('Personal Page'),

            bottom: TabBar(tabs: <Widget>[

              Tab(
                icon: Icon(Icons.alarm),
                text: 'Reminders',
              ),
              Tab(
                icon: Icon(Icons.note),
                text: 'Notes',
              ),
              Tab(
                icon: Icon(Icons.list),
                text: 'To-Do Lists',
              ),
            ]),
            automaticallyImplyLeading: true,
          ),
          body: IndexedStack(
              children: <Widget>[
                Container(
                    child: TabBarView(
                      children: <Widget>[
                        RemindersPanel(),
                        NotesPanel(),
                        TDListsPanel(),
                      ],
                    )
                ),
              ],
            ),
        )
    );
  }
}

class RemindersPanel extends StatefulWidget{

  @override
  RemindersApp createState() => RemindersApp();
}

class NotesPanel extends StatefulWidget{

  @override
  NotesApp createState() {
    return NotesApp();
  }
}

class TDListsPanel extends StatefulWidget{

  @override
  TDListsApp createState() {
    return TDListsApp();
  }
}

//Reminder Bloc

DateTime convertDateFromString(String strDate){
  DateTime todayDate = DateTime.parse(strDate);
  print(todayDate);
  print(formatDate(todayDate, [yyyy, '-', mm, '-', dd, ' ', hh, ':', nn, ':', ss, ' ', am]));
  return DateTime.parse(formatDate(todayDate, [yyyy, '-', mm, '-', dd, ' ', hh, ':', nn, ':', ss]));
}

class AddReminderPanel extends StatefulWidget {

  @override
  AddReminderState createState() => AddReminderState();

  void setReminder(Reminders r){

  }
}

int id = 1;
class AddReminderState extends State<AddReminderPanel> with AutomaticKeepAliveClientMixin<AddReminderPanel> {
  @override
  bool get wantKeepAlive => true;


  final bloc = ReminderBloc();
  String _date = "";
  String _time = "";
  DateTime _dateTime;

  TextEditingController _textEditingController = new TextEditingController();
  final notifications = FlutterLocalNotificationsPlugin();

  @override
  void initState() {
    super.initState();
    final settingsAndroid = AndroidInitializationSettings('logo');
    final settingsIOS = IOSInitializationSettings(
        onDidReceiveLocalNotification: (id, title, body, payload) =>
            onSelectNotification(payload));

    notifications.initialize(
        InitializationSettings(settingsAndroid, settingsIOS),
        onSelectNotification: onSelectNotification);
  }

  Future onSelectNotification(String payload) async => await Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => HomePage(payload: payload)),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: GradientAppBar(
        gradient: LinearGradient(colors: [Colors.pinkAccent[100], Colors.deepPurpleAccent[200]]),
        title: Text('Set Reminder'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: 130.0),
              Text(
                "Reminder",
                style: TextStyle(fontSize: 15.0),
              ),
              TextFormField(
                autocorrect: false,
                controller: _textEditingController,
                decoration: InputDecoration(hintText: "Type..."),
              ),
              SizedBox(height: 50),
              RaisedButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0)),
                elevation: 4.0,
                onPressed: () {
                  DatePicker.showDatePicker(context,
                      theme: DatePickerTheme(
                        containerHeight: 210.0,
                      ),
                      showTitleActions: true,
                      minTime: DateTime.now(),
                      maxTime: DateTime(2022, 12, 31), onConfirm: (date) {
                        print('confirm $date');
                        var mon = '${date.month}';
                        var d = '${date.day}';

                        if(int.parse(mon) < 10) mon = '0' + mon;
                        if(int.parse(d) < 10) d = '0' + d;

                        _date = '${date.year}-' + mon + '-' + d;
                        setState(() {});
                      }, currentTime: DateTime.now(), locale: LocaleType.en);
                },
                child: Container(
                  alignment: Alignment.center,
                  height: 50.0,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Container(
                            child: Row(
                              children: <Widget>[
                                Icon(
                                  Icons.date_range,
                                  size: 18.0,
                                  color: Colors.pinkAccent[100],
                                ),
                                Text(
                                  " $_date",
                                  style: TextStyle(
                                      color: Colors.pinkAccent[100],
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18.0),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                      Text(
                        "  Change",
                        style: TextStyle(
                            color: Colors.deepPurpleAccent[100],
                            fontWeight: FontWeight.bold,
                            fontSize: 18.0),
                      ),
                    ],
                  ),
                ),
                color: Colors.white,
              ),
              SizedBox(
                height: 20.0,
              ),
              RaisedButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0)),
                elevation: 4.0,
                onPressed: () {
                  DatePicker.showTimePicker(context,
                      theme: DatePickerTheme(
                        containerHeight: 210.0,
                      ),
                      showTitleActions: true, onConfirm: (time) {
                        print('confirm $time');
                        var hour = '${time.hour}';
                        var min = '${time.minute}';

                        if(int.parse(hour) < 10) hour = '0' + hour;
                        if(int.parse(min) < 10) min = '0' + min;

                        _time = hour + ':' + min;
                        setState(() {});
                      }, currentTime: DateTime.now(), locale: LocaleType.en);
                  setState(() {});
                },
                child: Container(
                  alignment: Alignment.center,
                  height: 50.0,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Container(
                            child: Row(
                              children: <Widget>[
                                Icon(
                                  Icons.access_time,
                                  size: 18.0,
                                  color: Colors.pinkAccent[100],
                                ),
                                Text(
                                  " $_time",
                                  style: TextStyle(
                                      color: Colors.pinkAccent[100],
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18.0),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                      Text(
                        "  Change",
                        style: TextStyle(
                            color: Colors.deepPurpleAccent[100],
                            fontWeight: FontWeight.bold,
                            fontSize: 18.0),
                      ),
                    ],
                  ),
                ),
                color: Colors.white,
              ),
              SizedBox(height: 30.0),
              RaisedButton(
                color: Colors.deepPurpleAccent[100],
                child: Text("DONE", style: TextStyle(color: Colors.white)),
                onPressed: () async {
                  _dateTime = DateTime.parse(_date + ' ' + _time);

                  await bloc.add(Reminders(name: _textEditingController.text, date: _date,
                      time: _time, date_modified: DateFormat('kk:mm:ss EEE MMM d y')
                          .format(DateTime.now())));
                  await showOngoingNotification(notifications, id: id,
                      title: "Reminder from ProcrastiNOT",
                      body: _textEditingController.text, time: _dateTime);
                  id++;
                  await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (BuildContext context) => PersonalPage(),
                        fullscreenDialog: true,
                      )
                  );
                  setState(() {Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (BuildContext context) => HomePage(),
                        fullscreenDialog: true,
                      )
                  );});
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class RemindersApp extends State<RemindersPanel> with AutomaticKeepAliveClientMixin<RemindersPanel>{
  final String payload;

  RemindersApp({
    @required this.payload,
    Key key,
  }) : super();

  @override
  bool get wantKeepAlive => true;

  final bloc = ReminderBloc();
  final notifications = FlutterLocalNotificationsPlugin();


  TextEditingController _textEditingController = new TextEditingController();
  DateTime _dateTime;

  EditReminderFormState(Reminders r){
    return _textEditingController = TextEditingController(text: r.name);
  }

  @override
  void initState() {
    super.initState();

    final settingsAndroid = AndroidInitializationSettings('logo');
    final settingsIOS = IOSInitializationSettings(
        onDidReceiveLocalNotification: (id, title, body, payload) =>
            onSelectNotification(payload));

    notifications.initialize(
        InitializationSettings(settingsAndroid, settingsIOS),
        onSelectNotification: onSelectNotification);
  }

  Future onSelectNotification(String payload) async => await Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => HomePage(payload: payload)),
  );

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context){
    super.build(context);
    return Scaffold(
      body: Stack(
        //padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: ExactAssetImage('assets/bg3.jpg'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            StreamBuilder<List<Reminders>>(
              initialData: List<Reminders>(),
              stream: bloc.reminder,
              //padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
              builder: (BuildContext context, AsyncSnapshot<List<Reminders>> snapshot) {
                if (snapshot.hasData) {
                  return ListView.builder(
                      itemCount: snapshot?.data?.length ?? 0,
                      itemBuilder: (BuildContext context, int index) {
                        Reminders item = snapshot.data[index];
                        String _date = item.date;
                        String _time = item.time;
                        return Card(
                          color: Colors.transparent,
                          child: Dismissible(
                            key: UniqueKey(),
                            background: Container(color: Colors.red),
                            onDismissed: (direction) {
                              bloc.delete(item.reminderid);
                              notifications.cancel(item.reminderid);
                              Scaffold
                                  .of(context)
                                  .showSnackBar(SnackBar(content: Text("Reminder dismissed")));
                            },
                            child: ListTile(
                              title: Text(item.time, style: TextStyle(fontSize: 20.0,
                                  fontWeight: FontWeight.bold, color: Colors.white)),
                              subtitle: Text(item.name, style: TextStyle(color: Colors.white)),
                              leading: Icon(Icons.alarm_on, color: Colors.white),
                              trailing: IconButton(icon: Icon(Icons.edit, color: Colors.white),
                                  onPressed: () {
                                    return showDialog(context: context,
                                      builder: (context) {
                                        return Scaffold(
                                          appBar: GradientAppBar(
                                            gradient: LinearGradient(colors: [Colors.pinkAccent[100], Colors.deepPurpleAccent[200]]),
                                            title: Text('Set Reminder'),
                                          ),
                                          body: Padding(
                                            padding: const EdgeInsets.all(16.0),
                                            child: SingleChildScrollView(
                                              child: Column(
                                                mainAxisSize: MainAxisSize.max,
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: <Widget>[
                                                  SizedBox(height: 130.0),
                                                  Text(
                                                    "Reminder",
                                                    style: TextStyle(fontSize: 15.0),
                                                  ),
                                                  TextFormField(
                                                    autocorrect: false,
                                                    controller: EditReminderFormState(item),
                                                  ),
                                                  SizedBox(height: 50),
                                                  RaisedButton(
                                                    shape: RoundedRectangleBorder(
                                                        borderRadius: BorderRadius.circular(5.0)),
                                                    elevation: 4.0,
                                                    onPressed: () {
                                                      DatePicker.showDatePicker(context,
                                                          theme: DatePickerTheme(
                                                            containerHeight: 210.0,
                                                          ),
                                                          showTitleActions: true,
                                                          minTime: DateTime.now(),
                                                          maxTime: DateTime(2022, 12, 31), onConfirm: (date) {
                                                            print('confirm $date');
                                                            var mon = '${date.month}';
                                                            var d = '${date.day}';

                                                            if(int.parse(mon) < 10) mon = '0' + mon;
                                                            if(int.parse(d) < 10) d = '0' + d;

                                                            _date = '${date.year}-' + mon + '-' + d;
                                                            setState(() {});
                                                          }, currentTime: DateTime.now(), locale: LocaleType.en);
                                                    },
                                                    child: Container(
                                                      alignment: Alignment.center,
                                                      height: 50.0,
                                                      child: Row(
                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                        children: <Widget>[
                                                          Row(
                                                            children: <Widget>[
                                                              Container(
                                                                child: Row(
                                                                  children: <Widget>[
                                                                    Icon(
                                                                      Icons.date_range,
                                                                      size: 18.0,
                                                                      color: Colors.pinkAccent[100],
                                                                    ),
                                                                    Text(
                                                                      '   $_date',
                                                                      style: TextStyle(
                                                                          color: Colors.pinkAccent[100],
                                                                          fontWeight: FontWeight.bold,
                                                                          fontSize: 18.0),
                                                                    ),
                                                                  ],
                                                                ),
                                                              )
                                                            ],
                                                          ),
                                                          Text(
                                                            "  Change",
                                                            style: TextStyle(
                                                                color: Colors.deepPurpleAccent[100],
                                                                fontWeight: FontWeight.bold,
                                                                fontSize: 18.0),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                    color: Colors.white,
                                                  ),
                                                  SizedBox(
                                                    height: 20.0,
                                                  ),
                                                  RaisedButton(
                                                    shape: RoundedRectangleBorder(
                                                        borderRadius: BorderRadius.circular(5.0)),
                                                    elevation: 4.0,
                                                    onPressed: () {
                                                      DatePicker.showTimePicker(context,
                                                          theme: DatePickerTheme(
                                                            containerHeight: 210.0,
                                                          ),
                                                          showTitleActions: true, onConfirm: (time) {
                                                            print('confirm $time');
                                                            var hour = '${time.hour}';
                                                            var min = '${time.minute}';

                                                            if(int.parse(hour) < 10) hour = '0' + hour;
                                                            if(int.parse(min) < 10) min = '0' + min;

                                                            _time = hour + ':' + min;
                                                            setState(() {});
                                                          }, currentTime: DateTime.now(), locale: LocaleType.en);
                                                      setState(() {});
                                                    },
                                                    child: Container(
                                                      alignment: Alignment.center,
                                                      height: 50.0,
                                                      child: Row(
                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                        children: <Widget>[
                                                          Row(
                                                            children: <Widget>[
                                                              Container(
                                                                child: Row(
                                                                  children: <Widget>[
                                                                    Icon(
                                                                      Icons.access_time,
                                                                      size: 18.0,
                                                                      color: Colors.pinkAccent[100],
                                                                    ),
                                                                    Text(
                                                                      '   $_time',
                                                                      style: TextStyle(
                                                                          color: Colors.pinkAccent[100],
                                                                          fontWeight: FontWeight.bold,
                                                                          fontSize: 18.0),
                                                                    ),
                                                                  ],
                                                                ),
                                                              )
                                                            ],
                                                          ),
                                                          Text(
                                                            "  Change",
                                                            style: TextStyle(
                                                                color: Colors.deepPurpleAccent[100],
                                                                fontWeight: FontWeight.bold,
                                                                fontSize: 18.0),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                    color: Colors.white,
                                                  ),
                                                  SizedBox(height: 30.0),
                                                  RaisedButton(
                                                    color: Colors.deepPurpleAccent[100],
                                                    child: Text("DONE", style: TextStyle(color: Colors.white)),
                                                    onPressed: () async {
                                                      _dateTime = DateTime.parse(_date + ' ' + _time);
                                                      showOngoingNotification(notifications, id: item.reminderid,
                                                          title: "Reminder from ProcrastiNOT",
                                                          body: _textEditingController.text, time: _dateTime);
                                                      await bloc.update(Reminders(reminderid: item.reminderid,
                                                          name: _textEditingController.text, date: _date,
                                                          time: _time, date_modified: DateFormat('kk:mm:ss EEE MMM d y')
                                                              .format(DateTime.now())));
                                                      await Navigator.push(
                                                          context,
                                                          MaterialPageRoute(
                                                            builder: (BuildContext context) => PersonalPage(),
                                                            fullscreenDialog: true,
                                                          )
                                                      );
                                                      setState(() {Navigator.push(
                                                          context,
                                                          MaterialPageRoute(
                                                            builder: (BuildContext context) => HomePage(),
                                                            fullscreenDialog: true,
                                                          )
                                                      );});
                                                    },
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        );
                                      },
                                    );
                                  }),
                              // trailing: ,
                            ),
                          ),
                        );
                      }
                  );
                } else {
                  return Center(child: CircularProgressIndicator());
                }
              },
            ),
          ]
      ),
      floatingActionButton: FloatingActionButton.extended(
        heroTag: 'reminder',
        onPressed: () async {
          return showDialog(context: context,
          builder: (context) {
              return AddReminderPanel();
            },
          );
        },
        icon: Icon(Icons.add_alarm),
        label: Text('Add Reminder'),
        backgroundColor: Colors.transparent,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}

//Reminder Block

//Notes Block
class Constants{
  static const String Edit = "Edit";
  static const String View = "View";
  static const List<String> choices = <String>[
    Edit,
    View
  ];
}

class NotesApp extends State<NotesPanel> {
  final bloc = NotesBloc();
  List color = [
    Colors.pink[100],
    Colors.pink[200],
    Colors.pink[300],
    Colors.pink[400],
    Colors.deepPurpleAccent[100],
    Colors.deepPurpleAccent[200],
    Colors.deepPurple[400],
    Colors.deepPurple[500],
  ];


  TextEditingController _textEditingController;

  EditNoteFormState(Notes note){
    return _textEditingController = TextEditingController(text: note.text);
  }

  String init = "";
  @override
  void initState() {
    _textEditingController = new TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    bloc.dispose();
    _textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: ExactAssetImage('assets/bg3.jpg'),
                fit: BoxFit.cover,
              ),
            ),
          ),
          StreamBuilder<List<Notes>>(
            initialData: List<Notes>(),
            stream: bloc.notes,
            //padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
            builder: (BuildContext context, AsyncSnapshot<List<Notes>> snapshot) {
              if (snapshot.hasData) {
                return ListView.builder(
                    itemCount: snapshot.data.length,
                    itemBuilder: (BuildContext context, int index) {
                      Notes item = snapshot.data[index];
                      return Card(
                        color: Colors.white70,
                        child: Dismissible(
                          key: UniqueKey(),
                          background: Container(color: Colors.red),
                          onDismissed: (direction) {
                            bloc.delete(item.notesid);
                            Scaffold
                                .of(context)
                                .showSnackBar(SnackBar(content: Text("Note dismissed")));
                          },
                          child: ListTile(
                            title: Text(item.text),
                            subtitle: Text(item.date_modified),
                            leading: Icon(Icons.note),
                            trailing: IconButton(icon: Icon(Icons.edit),
                                onPressed: () {
                                  return showDialog(context: context,
                                      builder: (context) {
                                        return AlertDialog(
                                          content: SingleChildScrollView(
                                            child: TextFormField(
                                              maxLines: 1,
                                              autocorrect: false,
                                              controller: EditNoteFormState(item),
                                            ),
                                          ),
                                          actions: <Widget>[
                                            new FlatButton(onPressed: () {
                                              _textEditingController.clear();
                                              Navigator.of(context).pop();
                                            },
                                              child: new Text('CANCEL'),
                                              textColor: Colors.deepPurpleAccent[100],
                                            ),
                                            new RaisedButton(
                                              color: Colors.deepPurpleAccent[100],
                                              textColor: Colors.white,
                                              onPressed: () async {
                                                await bloc.update(Notes(notesid: item.notesid, text: _textEditingController.text,
                                                    date_modified: DateFormat('kk:mm:ss EEE MMM d y')
                                                        .format(DateTime.now())));
                                                _textEditingController.clear();
                                                Navigator.of(context).pop();
                                              },
                                              child: new Text('DONE'),
                                            ),
                                          ],
                                        );
                                      });
                                }),
                            // trailing: ,
                          ),
                        ),
                      );
                    }
                );
              } else {
                return Center(child: CircularProgressIndicator());
              }
            },
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton.extended(
        heroTag: 'note',
        onPressed: () async {
          return showDialog(context: context,
              builder: (context) {
                return AlertDialog(
                  content: SingleChildScrollView(
                    child: TextFormField(
                      maxLines: 3,
                      autocorrect: false,
                      controller: _textEditingController,
                      decoration: InputDecoration(hintText: "Type..."),
                    ),
                  ),
                  actions: <Widget>[
                    new FlatButton(onPressed: () {
                      _textEditingController.clear();
                      Navigator.of(context).pop();
                    },
                      child: new Text('CANCEL'),
                      textColor: Colors.deepPurpleAccent[100],
                    ),
                    new RaisedButton(
                      color: Colors.deepPurpleAccent[100],
                      textColor: Colors.white,
                      onPressed: () async {
                        await bloc.add(Notes(text: _textEditingController.text,
                            date_modified: DateFormat('kk:mm:ss EEE MMM d y')
                                .format(DateTime.now())));
                        _textEditingController.clear();
                        Navigator.of(context).pop();
                      },
                      child: new Text('DONE'),
                    ),
                  ],
                );
              });
        },
        icon: Icon(Icons.note_add),
        label: Text('Add Note'),
        backgroundColor: Colors.transparent,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}

//Notes Block

//List Block

class AddListDialog extends StatefulWidget {

  @override
  AddListDialogState createState(){
    return AddListDialogState();
  }
}
class ShowListDialog extends StatefulWidget {

  @override
  ShowListDialogState createState(){
    return ShowListDialogState();
  }
}


class AddListDialogState extends State<AddListDialog> {

  final bloc = ListsBloc();
  bool Val = false;
  List<ListItems> listitems = [];
  List<String> _list = [];

  TextEditingController _textEditingController;

  EditListsFormState(Lists l) {
    return _textEditingController = TextEditingController(text: l.title);
  }

  TextEditingController titleController = TextEditingController();
  TextEditingController contentController = TextEditingController();
  final TextEditingController TodoController = new TextEditingController();

  TextEditingController check1 = TextEditingController();

  FocusNode titleFocus = FocusNode();
  FocusNode contentFocus = FocusNode();
  FocusNode TodoFocus = FocusNode();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: GradientAppBar( //title: Text('Create New Note'),
        gradient: LinearGradient(colors: [Colors.pinkAccent[100], Colors.deepPurpleAccent[200]]),

      ),
      body: Stack(
          children: <Widget>[
            Column(
              children: <Widget>[
                TextField(
                  focusNode: titleFocus,
                  autofocus: true,
                  controller: titleController,
                  keyboardType: TextInputType.multiline,
                  maxLines: null,
                  onSubmitted: (text) {
                    titleFocus.unfocus();
                    FocusScope.of(context).requestFocus(contentFocus);
                  },
                  onChanged: (value) {},
                  textInputAction: TextInputAction.next,
                  style: TextStyle(
                      fontFamily: 'ZillaSlab',
                      fontSize: 32,
                      fontWeight: FontWeight.w700),
                  decoration: InputDecoration.collapsed(
                    hintText: 'Enter a title',
                    hintStyle: TextStyle(
                        color: Colors.grey.shade400,
                        fontSize: 32,
                        fontFamily: 'ZillaSlab',
                        fontWeight: FontWeight.w700),
                    border: InputBorder.none,
                  ),
                ),
                new TextField(
                  decoration: InputDecoration.collapsed(
                    hintText: 'Enter to do list',
                    hintStyle: TextStyle(
                        color: Colors.grey.shade400,
                        fontSize: 20,
                        fontWeight: FontWeight.w700),
                    border: InputBorder.none,
                  ),
                  controller: TodoController,
                  //autofocus: true,
                  onSubmitted: (text) {
                    _list.add(text);
                    TodoController.clear();

                    setState(() {});
                  },
                ),
                new Expanded(
                    child: new ListView.builder
                      (
                        itemCount: _list.length,
                        itemBuilder: (BuildContext context, int Index) {
                          return checkbox(_list[Index], false);
                        }
                    )
                ),
              ],
            ),
          ]
      ),
      bottomNavigationBar: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          RaisedButton(
            padding: const EdgeInsets.all(8.0),
            color: Colors.deepPurpleAccent[100],
            textColor: Colors.white,
            onPressed: () async {
              for(int x = 0; x < _list.length;x++)
              {
                  listitems.add(new ListItems(body:_list.elementAt(x),status:0, listsid: 0));
              }
              await bloc.add(Lists(title: titleController.text,
                  date_modified: DateFormat('kk:mm:ss EEE MMM d y').format(
                      DateTime.now())),listitems);
              TodoController.clear();
              await Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (BuildContext context) => PersonalPage(),
                    fullscreenDialog: true,
                  )
              );
              setState(() {
                Navigator.of(context).pop();
                setState(() {
                  Navigator.of(context).pop();
                });
              });
            },
            child: new Text("Save"),
          ),
          SizedBox(width: 100.0),
          FlatButton(
            textColor: Colors.deepPurpleAccent[100],
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: new Text("Cancel"),
          ),
        ],
      ),
    );
  }

}

class ShowListDialogState extends State<ShowListDialog>{

  final bloc = ListsBloc();
  List<bool> _status = [];
  bool Val = false;
  List<ListItems> listitems = List<ListItems>();
  List<String> _list;




    //(listitems.elementAt(0));
  TextEditingController _textEditingController;

  EditListsFormState(Lists l){

    return _textEditingController = TextEditingController(text: l.title);
  }

  TextEditingController titleController = TextEditingController();
  TextEditingController contentController = TextEditingController();
  final TextEditingController TodoController = new TextEditingController();

  TextEditingController check1=TextEditingController();

  FocusNode titleFocus = FocusNode();
  FocusNode contentFocus = FocusNode();
  FocusNode TodoFocus = FocusNode();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(//title: Text('Create New Note'),
      ),
      body: Stack(
          children:<Widget>[
            Column(
              children: <Widget>[
                TextField(
                 controller: titleController,
                 readOnly: true,
                  onChanged: (value) {
                  },
                  textInputAction: TextInputAction.next,
                  style: TextStyle(
                      fontFamily: 'ZillaSlab',
                      fontSize: 32,
                      fontWeight: FontWeight.w700),
                  decoration: InputDecoration.collapsed(
                    hintText: 'Enter a title',
                    hintStyle: TextStyle(
                        color: Colors.grey.shade400,
                        fontSize: 32,
                        fontFamily: 'ZillaSlab',
                        fontWeight: FontWeight.w700),
                    border: InputBorder.none,
                  ),
                ),
                CheckboxGroup(
                  labels: _list,
                  onChange: (bool isChecked, String label, int index) => print("isChecked: $isChecked   label: $label  index: $index"),
                  onSelected: (List<String> checked) => print("checked: ${checked.toString()}"),
                ),
              ],
            ),
          ]
      ),
      bottomNavigationBar: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          RaisedButton(
            padding: const EdgeInsets.all(8.0),
            textColor: Colors.white,
            color: Colors.lightGreen,
            onPressed: () async{
              await bloc.add(Lists(title:titleController.text,
                  date_modified:DateFormat('kk:mm:ss EEE MMM d y').format(DateTime.now())),
                  listitems);
              TodoController.clear();
              await Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (BuildContext context) => PersonalPage(),
                    fullscreenDialog: true,
                  )
              );
              setState(() {Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (BuildContext context) => HomePage(),
                    fullscreenDialog: true,
                  )
              );});
            },
            child: new Text("Save"),
          ),
          SizedBox(width: 100.0),
          RaisedButton(
            padding: const EdgeInsets.all(8.0),
            textColor: Colors.white,
            color: Colors.redAccent,
            onPressed: (){
              Navigator.of(context).pop();
            },
            child: new Text("Cancel"),
          ),
        ],
      ),
    );
  }
}

Widget checkbox(String title, bool boolValue) {
  return CheckboxListTile(


    title: Text(title), //    <-- label
    value: boolValue,
    onChanged: (newValue) {

    },
  );

}
class TDListsApp extends State<TDListsPanel> with AutomaticKeepAliveClientMixin<TDListsPanel>{
  final bloc = ListsBloc();
  final blocitems = ListItemsBloc();
  bool Val = false;
  List<bool> value = List<bool>();
  List<String> _list;
  @override
  bool get wantKeepAlive => true;

  TextEditingController _textEditingController;
  TextEditingController _titleController;

  EditTitleFormState(Lists l){
    return _titleController = TextEditingController(text: l.title);
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: Stack(
        //padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: ExactAssetImage('assets/bg3.jpg'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            StreamBuilder<List<Lists>>(
              initialData: List<Lists>(),
              stream: bloc.lists,
              //padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
              builder: (BuildContext context, AsyncSnapshot<List<Lists>> snapshot) {
                if (snapshot.hasData) {
                  return ListView.builder(
                      itemCount: snapshot.data == null ? 0 : snapshot.data.length,
                      itemBuilder: (BuildContext context, int index) {
                        Lists item = snapshot.data[index];
                        return Card(
                          color: Colors.white70,
                          child: Dismissible(
                            key: UniqueKey(),
                            background: Container(color: Colors.red),
                            onDismissed: (direction) {
                              bloc.delete(item.listsid);
                              Scaffold
                                  .of(context)
                                  .showSnackBar(SnackBar(content: Text("To-Do List dismissed")));
                            },
                            child: ListTile(
                              title: Text(item.title.toString()),
                              onTap: (){
                                //debugPrint("taaaaap");
                                return showDialog(context: context,
                                    builder: (context) {
                                  //  return ShowListDialog();
                                      return Scaffold(
                                        appBar: GradientAppBar(//title: Text('Create New Note'),
                                          gradient: LinearGradient(colors: [Colors.pinkAccent[100], Colors.deepPurpleAccent[200]]),
                                        ),
                                        body: ListView(
                                            children:<Widget>[
                                              Column(
                                                children: <Widget>[
                                                  TextField(
                                                     controller: EditTitleFormState(item),
                                                    readOnly: true,
                                                    onChanged: (value) {
                                                    },
                                                    textInputAction: TextInputAction.next,
                                                    style: TextStyle(
                                                        fontFamily: 'ZillaSlab',
                                                        fontSize: 32,
                                                        fontWeight: FontWeight.w700),
                                                    decoration: InputDecoration.collapsed(
                                                      hintText: 'Enter a title',
                                                      hintStyle: TextStyle(
                                                          color: Colors.grey.shade400,
                                                          fontSize: 32,
                                                          fontFamily: 'ZillaSlab',
                                                          fontWeight: FontWeight.w700),
                                                      border: InputBorder.none,
                                                    ),
                                                  ),
                                                  StreamBuilder(
                                                    stream: blocitems.listitems,
                                                      builder: (BuildContext context, AsyncSnapshot<List<ListItems>> snapshot) {
                                                        if (snapshot.hasData) {
                                                          return ListView.builder(
                                                            scrollDirection: Axis.vertical,
                                                            shrinkWrap: true,
                                                            padding: EdgeInsets.all(5.0),
                                                            itemCount: snapshot.data == null ? 0 : snapshot.data.length,
                                                            itemBuilder: (BuildContext context, int index) {
                                                              ListItems project = snapshot.data[index];
                                                              debugPrint(item.listsid.toString());
                                                              debugPrint(project.body);
                                                              return CheckboxListTile(
                                                                title: Text(project.body),
                                                                value: Val,
                                                                onChanged: (bool value)
                                                                {
                                                                  setState(() {
                                                                    Val = value;
                                                                  });
                                                                },
                                                              );
                                                            },
                                                          );
                                                        } else {
                                                          return Center(child: CircularProgressIndicator());
                                                        }
                                                      },
                                                    ),
                                                ],
                                              ),
                                            ]
                                        ),
                                        bottomNavigationBar: Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          mainAxisSize: MainAxisSize.max,
                                          children: <Widget>[
                                            RaisedButton(
                                              padding: const EdgeInsets.all(8.0),
                                              color: Colors.deepPurpleAccent[100],
                                              textColor: Colors.white,
                                              onPressed: () async{
//                                          await bloc.add(Lists(title:titleController.text,
//                                              date_modified:DateFormat('kk:mm:ss EEE MMM d y').format(DateTime.now())),
//                                              listitems);
//                                              TodoController.clear();
                                                await Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                      builder: (BuildContext context) => PersonalPage(),
                                                      fullscreenDialog: true,
                                                    )
                                                );
                                                setState(() {Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                      builder: (BuildContext context) => HomePage(),
                                                      fullscreenDialog: true,
                                                    )
                                                );});
                                              },
                                              child: new Text("Save"),
                                            ),
                                            SizedBox(width: 100.0),
                                            FlatButton(
                                              textColor: Colors.deepPurpleAccent[100],
                                              onPressed: (){
                                                Navigator.of(context).pop();
                                              },
                                              child: new Text("Cancel"),
                                            ),
                                          ],
                                        ),
                                      );

                                    });
                              },
                              subtitle: Text(item.date_modified),
                              leading: Icon(Icons.list),
//                              trailing: IconButton(icon: Icon(Icons.edit),
////                                  onPressed: () {
////                                    return showDialog(context: context,
////                                        builder: (context) {
////                                          return AlertDialog(
////                                            content: SingleChildScrollView(
////                                              child: TextFormField(
////                                                maxLines: 1,
//                                                autocorrect: false,
//                                                controller: EditListsFormState(item),
//                                              ),
//                                            ),
//                                            actions: <Widget>[
//                                              new FlatButton(onPressed: () {
//                                                _textEditingController.clear();
//                                                Navigator.of(context).pop();
//                                              },
//                                                child: new Text('CANCEL'),
//                                                textColor: Colors.deepPurpleAccent[100],
//                                              ),
//                                              new RaisedButton(
//                                                color: Colors.deepPurpleAccent[100],
//                                                textColor: Colors.white,
//                                                onPressed: () async {
//                                                  _textEditingController.clear();
//                                                  Navigator.of(context).pop();
//                                                },
//                                                child: new Text('DONE'),
//                                              ),
//                                            ],
//                                          );
//                                        });
//                                  }),
                              // trailing: ,
                            ),

                          ),
                        );
                      }
                  );
                } else {
                  return Center(child: CircularProgressIndicator());
                }
              },
            ),
          ]
      ),
      floatingActionButton: FloatingActionButton.extended(
        heroTag: 'list',
        onPressed: () async {
          return showDialog(context: context,
              builder: (context) {
                 return AddListDialog();
              });
        },
        icon: Icon(Icons.list),
        label: Text('Add List'),
        backgroundColor: Colors.transparent,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}

Widget Addcheckbox(String title, bool boolValue) {
  return CheckboxListTile(

    title: Text(title), //    <-- label
    value: boolValue,
    onChanged: (newValue) {
    },
  );
}

class _onTapTileDialog extends StatefulWidget {

  @override
  _ontapTileDialogState createState(){
    return _ontapTileDialogState();
  }
}

class _ontapTileDialogState extends State<_onTapTileDialog>{

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Padding(
           child: ListView(
             children: <Widget>[
               Padding(
                 padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
                 child: TextField(
                  // controller: titleController,
                 //  style: textStyle,
                   onChanged: (value) {
                     debugPrint('Something changed in Title Text Field');
                   },
                   decoration: InputDecoration(
                       labelText: 'Title',
                       //labelStyle: textStyle,
                       border: OutlineInputBorder(
                           borderRadius: BorderRadius.circular(5.0)
                       )
                   ),
                 ),
               ),

               // Third Element
               Padding(
                 padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
                 child: TextField(
                  // controller: descriptionController,
                   //style: textStyle,
                   onChanged: (value) {
                     debugPrint('Something changed in Description Text Field');
                   },
                   decoration: InputDecoration(
                       labelText: 'Description',
                      // labelStyle: textStyle,
                       border: OutlineInputBorder(
                           borderRadius: BorderRadius.circular(5.0)
                       )
                   ),
                 ),
               ),
             ],
           )
      ),

    );

  }



}


//List Block