import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_procrastinot/ui/router.dart';
import 'package:flutter_procrastinot/ui/views/login.dart';
import 'package:flutter_procrastinot/ui/widgets/local_push_notifications.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

void main(){
  runZoned<Future<Null>>(() async {
    runApp(MyApp());
  });
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: LoginPage(),
        title: 'ProcrastiNOT',
        theme: ThemeData(),
        onGenerateRoute: Router.generateRoute,
   );

  }
}

//NO NUT TATS
//void main() => runApp(MyApp());
//
// ///CHA CODE///
//class MyApp extends StatelessWidget {
//  final String appTitle = 'Local Notifications';
//  @override
//  Widget build(BuildContext context) => MaterialApp(
//    title: appTitle,
//    home: MainPage(appTitle: appTitle),
//  );
//}
//
//class MainPage extends StatelessWidget {
//  final String appTitle;
//
//  const MainPage({this.appTitle});
//
//  @override
//  Widget build(BuildContext context) => Scaffold(
//    appBar: AppBar(
//      title: Text(appTitle),
//    ),
//    body: Padding(
//      padding: const EdgeInsets.all(8.0),
//      child: LocalNotificationWidget(),
//    ),
//  );
//}

//////MICO CODE////
//
//
//class MyApp extends StatefulWidget{
//
//  @override
//  _MyAppState createState() =>_MyAppState();
//}
//class MainPage extends StatelessWidget {
//  final String appTitle;
//
//  const MainPage({this.appTitle});
//
//  @override
//  Widget build(BuildContext context) => Scaffold(
//    appBar: AppBar(
//      title: Text(appTitle),
//    ),
//    body: Padding(
//      padding: const EdgeInsets.all(8.0),
//      child: LocalNotificationWidget(),
//    ),
//  );
//}
//
//class _MyAppState extends State<MyApp>{
//  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
//
//  @override
//  void initState() {
//    // TODO: implement initState
//    super.initState();
//    flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
//    var android = new AndroidInitializationSettings('@mipmap/ic_launcher');
//    var ios = new IOSInitializationSettings();
//    var initSettings = new InitializationSettings(android,ios);
//    flutterLocalNotificationsPlugin.initialize(initSettings, selectNotification, onSelectNotification);
//  }
//  Future onSelectNotification(String payload)
//  {
//    debugPrint("Payload : $payload");
//    showDialog(context: context, builder: (_)=> AlertDialog(
//      title: new Text('Notification'),
//      content: new Text('Payload')
//    ));
//  }
//
//  @override
//  Widget build(BuildContext context)
//  {
//    return Scaffold(
//      appBar: new AppBar(
//        title: new Text('Flutter Local Notification'),
//      ),
//      body: new Center(
//        child: new RaisedButton(
//          onPressed: showNotification,
//          child: new Text(
//            'DemoIGIT',
//            style: Theme.of(context).textTheme.headline,
//          )
//      )
//      )
//    );
//  }
//  showNotification() async
//  {
//    var android = new AndroidNotificationDetails('channel id', 'channel NAME', 'CHANNEL DISCRIPTION');
//    var iOS = new IOSNotificationDetails();
//    var platform =new NotificationDetails(android, iOS);
//    await flutterLocalNotificationsPlugin.show(0, 'IGIT GUD IGIT', 'Flutter Local Notif', platform);
//
//
//
//  }
//}

//import 'dart:async';
//import 'package:flutter/material.dart';
//import 'package:flutter_local_notifications/flutter_local_notifications.dart';
//
//void main() {
//  runApp(
//    new MaterialApp(home: new MyApp()),
//  );
//}
//
//class MyApp extends StatefulWidget {
//  @override
//  _MyAppState createState() => new _MyAppState();
//}
//
//class _MyAppState extends State<MyApp> {
//  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
//
//  @override
//  initState() {
//    super.initState();
//    // initialise the plugin. app_icon needs to be a added as a drawable resource to the Android head project
//    // If you have skipped STEP 3 then change app_icon to @mipmap/ic_launcher
//    var initializationSettingsAndroid =
//    new AndroidInitializationSettings('logo');
//    var initializationSettingsIOS = new IOSInitializationSettings();
//    var initializationSettings = new InitializationSettings(
//        initializationSettingsAndroid, initializationSettingsIOS);
//    flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
//    flutterLocalNotificationsPlugin.initialize(initializationSettings,
//        onSelectNotification: onSelectNotification);
//
////    var scheduledNotificationDateTime =
////    new DateTime.now().add(new Duration(seconds: 5));
////    var androidPlatformChannelSpecifics =
////    new AndroidNotificationDetails('your other channel id',
////        'your other channel name', 'your other channel description');
////    var iOSPlatformChannelSpecifics =
////    new IOSNotificationDetails();
////    NotificationDetails platformChannelSpecifics = new NotificationDetails(
////        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
////    await flutterLocalNotificationsPlugin.schedule(
////        0,
////        'scheduled title',
////        'scheduled body',
////        scheduledNotificationDateTime,
////        platformChannelSpecifics);
//  }
//
//  Future _showNotificationWithSound() async {
//    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
//        'your channel id', 'your channel name', 'your channel description',
//        sound: 'slow_spring_board',
//        importance: Importance.Max,
//        priority: Priority.High);
//    var iOSPlatformChannelSpecifics =
//    new IOSNotificationDetails(sound: "slow_spring_board.aiff");
//    var platformChannelSpecifics = new NotificationDetails(
//        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
//    await flutterLocalNotificationsPlugin.show(
//      0,
//      'New Post',
//      'How to Show Notification in Flutter',
//      platformChannelSpecifics,
//      payload: 'Custom_Sound',
//    );
//  }
//// Method 2
//  Future _showNotificationWithDefaultSound() async {
//    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
//        'your channel id', 'your channel name', 'your channel description',
//        importance: Importance.Max, priority: Priority.High);
//    var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
//    var platformChannelSpecifics = new NotificationDetails(
//        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
//    await flutterLocalNotificationsPlugin.show(
//      0,
//      'New Post',
//      'How to Show Notification in Flutter',
//      platformChannelSpecifics,
//      payload: 'Default_Sound',
//    );
//  }
//// Method 3
//  Future _showNotificationWithoutSound() async {
//    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
//        'your channel id', 'your channel name', 'your channel description',
//        playSound: false, importance: Importance.Max, priority: Priority.High);
//    var iOSPlatformChannelSpecifics =
//    new IOSNotificationDetails(presentSound: false);
//    var platformChannelSpecifics = new NotificationDetails(
//        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
//    await flutterLocalNotificationsPlugin.show(
//      0,
//      'New Post',
//      'How to Show Notification in Flutter',
//      platformChannelSpecifics,
//      payload: 'No_Sound',
//    );
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return new MaterialApp(
//      home: new Scaffold(
//        appBar: new AppBar(
//          title: new Text('Plugin example app'),
//        ),
//        body: new Center(
//          child: new Column(
//            mainAxisAlignment: MainAxisAlignment.center,
//            mainAxisSize: MainAxisSize.max,
//            children: <Widget>[
//              new RaisedButton(
//                onPressed: _showNotificationWithSound,
//                child: new Text('Show Notification With Sound'),
//              ),
//              new SizedBox(
//                height: 30.0,
//              ),
//              new RaisedButton(
//                onPressed: _showNotificationWithoutSound,
//                child: new Text('Show Notification Without Sound'),
//              ),
//              new SizedBox(
//                height: 30.0,
//              ),
//              new RaisedButton(
//                onPressed: _showNotificationWithDefaultSound,
//                child: new Text('Show Notification With Default Sound'),
//              ),
//            ],
//          ),
//        ),
//      ),
//    );
//  }
//
//  Future onSelectNotification(String payload) async {
//    showDialog(
//      context: context,
//      builder: (_) {
//        return new AlertDialog(
//          title: Text("PayLoad"),
//          content: Text("Payload : $payload"),
//        );
//      },
//    );
//  }
//}