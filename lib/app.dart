//import 'package:flutter/material.dart';
//import 'ui/views/home.dart';
//import 'ui/views/login.dart';
//
//class ProjectApp extends StatelessWidget {
//
//  @override
//  Widget build(BuildContext context){
//    return MaterialApp(
//      title: 'ProcrastiNOT',
//      home: HomePage(),
//      initialRoute: '/login',
//      onGenerateRoute: _getRoute,
//    );
//  }
//
//  Route<dynamic> _getRoute(RouteSettings settings){
//    if(settings.name != '/login'){
//      return null;
//    }
//
//    return MaterialPageRoute<void>(
//      settings: settings,
//      builder: (BuildContext context) => LoginPage(),
//      fullscreenDialog: true,
//    );
//  }
//}