import 'dart:async';
import 'package:firebase_database/firebase_database.dart';
import 'package:intl/intl.dart';


class Project
{
  String projectName;
  String notes;
  String reminder;
  List<bool> status;
  List<String> list;
  String date_modified;
  String person_modified;
  Project(String projectName,String notes,String reminder,List<bool> status,List<String> list,String date_modified,String person_modified)
  {
    this.projectName = projectName;
    this.notes = notes;
    this.reminder = reminder;
    this.status = status;
    this.list = list;
    this.date_modified = date_modified;
    this.person_modified = person_modified;
  }

  Project.fromJson(Map data)
  {
    projectName = (projectName == null )? '': data['project name'];
    notes = (notes == null )? '': data['notes'];
    reminder = (reminder == null )? '': data['reminder'];
    status = (status == null )? [false] : data['status'];
    list = (list == null )? ['']: data['list'];
    date_modified = (date_modified == null )?  DateFormat('kk:mm:ss EEE MMM d y').format(DateTime.now()): data['date_modified'];
    person_modified = (person_modified == null )? '': data['person_modified'];
  }
}

class CRUDProject {

  static Future<Query> queryProjects(String password) async {

    return FirebaseDatabase.instance
        .reference()
        .child("Accounts")
        .child(password);
  }
//Retrieve Selected Project
  static Future<StreamSubscription<Event>> getProjectStream(String password,String projectTitle,
      void onData(Project project)) async {

    StreamSubscription<Event> subscription = FirebaseDatabase.instance
        .reference()
        .child("Accounts")
        .child(password)
        .child(projectTitle)
        .onValue
        .listen((Event event) {
      var todo = new Project.fromJson(event.snapshot.value);
      onData(todo);
    });
    return subscription;
  }


  static Future<Project> getProject(String password,String projectTitle,) async {
    Completer<Project> completer = new Completer<Project>();

    FirebaseDatabase.instance
        .reference()
        .child("Accounts")
        .child(password)
        .child(projectTitle)
        .once()
        .then((DataSnapshot snapshot) {
      var project = new Project.fromJson(snapshot.value);
      completer.complete(project);
    });
    return completer.future;
  }

  //create
  static void createFirebaseDatabase(String password,String projectTitle,Project p) async {

    var project = <String, dynamic>{
      'project name': projectTitle,
      'notes' : p.notes,
      'reminder' : p.reminder,
      'status' : p.status,
      'list' : p.list,
      'person modified': p.person_modified,
      'date modified' : p.date_modified
    };

    DatabaseReference reference = FirebaseDatabase.instance
        .reference()
        .child("Accounts")
        .child(password)
        .child(projectTitle);

    reference.set(project);
  }

  //update
  static void updateFirebaseDatabase(String password,String projectTitle,Project p) async {
    DatabaseReference reference = FirebaseDatabase.instance.reference();

    var project = <String, dynamic>{
      'project name': projectTitle,
      'notes' : p.notes,
      'reminder' : p.reminder,
      'status' : p.status,
      'list' : p.list,
      'person modified': p.person_modified,
      'date modified' : p.date_modified
    };

    await reference.
    child("Accounts").
    child(password).
    child(projectTitle).
    update(project);
  }

  //delete
  static void deleteFirebaseDatabase(String password,String projectTitle) async {
    DatabaseReference reference = FirebaseDatabase.instance.reference();
    await reference..child("Accounts").child(password).child(projectTitle).remove();
  }
}