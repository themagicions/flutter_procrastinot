import 'dart:io';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';


initDB() async {
  Directory documentsDirectory = await getApplicationDocumentsDirectory();
  String path = join(documentsDirectory.path, 'ProcrastiNOT.db');
  return await openDatabase(path, version: 1, onOpen: (db) {
  }, onCreate: (Database db, int version) async {
    await db.execute("CREATE TABLE Reminders("
        "reminderid INTEGER PRIMARY KEY, "
        "name TEXT, "
        "date TEXT, "
        "time TEXT, "
        "date_modified TEXT"
        ")");
    await db.execute("CREATE TABLE Notes("
        "notesid INTEGER PRIMARY KEY, "
        "text TEXT, "
        "date_modified TEXT"
        ")");

    //one to many
    await db.execute("CREATE TABLE Lists("
        "listsid INTEGER PRIMARY KEY, "
        "title TEXT, "
        "date_modified TEXT"
        ")");
    await db.execute("CREATE TABLE ListItems("
        "listitemsid INTEGER PRIMARY KEY, "
        "body TEXT, "
        "status BOOLEAN,"
        "listsid INTEGER,"
        "FOREIGN KEY(listsid)"
        "REFERENCES Lists(listsid)"
        " ON DELETE CASCADE "
        ")");
    //one to many
/*
    //one to many
    await db.execute("CREATE TABLE Team("
        "teamid INTEGER PRIMARY KEY, "
        "team_name TEXT, "
        "date_modified TEXT"
        ")");
    await db.execute("CREATE TABLE Person("
        "personid INTEGER PRIMARY KEY, "
        "email TEXT, "
        "name TEXT,"
        "teamid INTEGER,"
        "FOREIGN KEY (teamid)"
        "REFERENCES Lists(teamid)"
        ")");
    //one to many

    //joined table
    await db.execute("CREATE TABLE Project("
        "projectid INTEGER PRIMARY KEY, "
        "project_name TEXT, "
        "teamid INTEGER, "
        "notesid INTEGER, "
        "reminderid INTEGER, "
        "listsid INTEGER, "
        "FOREIGN KEY (teamid) "
        "REFERENCES Team(teamid), "
        "FOREIGN KEY (notesid) "
        "REFERENCES Notes(notesid), "
        "FOREIGN KEY (reminderid) "
        "REFERENCES Reminders(reminderid), "
        "FOREIGN KEY (listsid) "
        "REFERENCES Lists(listsid) "
        ")");
    //joined table*/
  });
}