import 'dart:async';
import 'package:sqflite/sqflite.dart';
import 'package:flutter_procrastinot/core/models/offlineModels/notes.dart';
import 'database_creator.dart';


class DBProvider {
  DBProvider._();
  static final DBProvider db = DBProvider._();
  static Database _database;


  Future<Database> get database async {
    if (_database != null) {
      return _database;
    }
    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  newNotes(Notes newNotes) async {
    final db = await database;
    //get the biggest id in the table
    var table = await db.rawQuery("SELECT MAX(notesid)+1 as notesid FROM Notes");
    int id = table.first["notesid"];
    //insert to the table using the new id
    var raw = await db.rawInsert(
        "INSERT Into Notes (notesid,text,date_modified)"
            " VALUES (?,?,?)",
        [id, newNotes.text,newNotes.date_modified]);
    return raw;
  }

  getNotes(int id) async {
    final db = await database;
    var res =await  db.query("Notes", where: "noteid = ?", whereArgs: [id]);
    return res.isNotEmpty ? Notes.fromJson(res.first) : Null ;
  }

  Future<List<Notes>> getAllNotes() async {
    final db = await database;
    var res = await db.query("Notes");
    List<Notes> list =
    res.isNotEmpty ? res.map((c) => Notes.fromJson(c)).toList() : [];
    return list;
  }

  Future<void> updateNotes(Notes newNotes) async {
    final db = await database;
//    return res;
    var raw = await db.rawUpdate('''
        UPDATE Notes
         SET text = ?, date_modified = ?
         WHERE notesid = ?
            ''',
        [newNotes.text, newNotes.date_modified, newNotes.notesid]);
    return raw;
  }

  deleteNotes(int id) async {
    final db = await database;
    db.delete("Notes", where: "notesid = ?", whereArgs: [id]);
  }
}

class NotesBloc {

  NotesBloc() {
    getNotes();
  }
  final _noteController =  StreamController<List<Notes>>.broadcast();
  get notes => _noteController.stream;

  dispose() {
    _noteController.close();
  }


  getNotes() async {
    _noteController.sink.add(await DBProvider.db.getAllNotes());
  }

  getSelectedNotes(int id) async {
    _noteController.sink.add(await DBProvider.db.getNotes(id));
  }

  delete(int id) async {
    await DBProvider.db.deleteNotes(id);
    getNotes();
  }

  add(Notes notes) async {
    await DBProvider.db.newNotes(notes);
    getNotes();
  }

  update(Notes notes) async {
    await DBProvider.db.updateNotes(notes);
    getNotes();
  }
}



