import 'dart:async';
import 'package:flutter_procrastinot/core/models/offlineModels/reminder.dart';
import 'package:sqflite/sqflite.dart';
import 'database_creator.dart';

class DBProvider {
  DBProvider._();
  static final DBProvider db = DBProvider._();
  static Database _database;

  Future<Database> get database async {
    if (_database != null) {
      return _database;
    }
    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  newReminder(Reminders newReminder) async {
    final db = await database;
    //get the biggest id in the table
    var table = await db
        .rawQuery("SELECT MAX(reminderid)+1 as reminderid FROM Reminders");
    int id = table.first["reminderid"];
    //insert to the table using the new id
    var raw = await db.rawInsert(
        "INSERT Into Reminders (reminderid,name,date,time,date_modified)"
        " VALUES (?,?,?,?,?)",
        [id, newReminder.name, newReminder.date, newReminder.time, newReminder.date_modified]);
    return raw;
  }

  getReminder(int id) async {
    final db = await database;
    var res =
        await db.query("Reminders", where: "reminderid = ?", whereArgs: [id]);
    return res.isNotEmpty ? Reminders.fromJson(res.first) : Null;
  }

  Future<List<Reminders>> getAllReminder() async {
    final db = await database;
    var res = await db.query("Reminders");
    List<Reminders> list =
        res.isNotEmpty ? res.map((c) => Reminders.fromJson(c)).toList() : [];
    return list;
  }

  updateReminder(Reminders newReminder) async {
    final db = await database;
    var res = await db.update("Reminders", newReminder.toJson(),
        where: "reminderid = ?", whereArgs: [newReminder.reminderid]);
    return res;
  }

  deleteReminder(int id) async {
    final db = await database;
    db.delete("Reminders", where: "reminderid = ?", whereArgs: [id]);
  }

  getId(String name) async {
    final db = await database;
    var res =
    await db.query("Reminders", where: "name = ?", whereArgs: [name]);
    return res.isNotEmpty ? Reminders.fromJson(res.first) : Null;
  }

}

class ReminderBloc {
  ReminderBloc() {
    getReminder();
  }
  final _reminderController = StreamController<List<Reminders>>.broadcast();
  get reminder => _reminderController.stream;

  dispose() {
    _reminderController.close();
  }

  getReminder() async {
    _reminderController.sink.add(await DBProvider.db.getAllReminder());
  }

  getSelectedReminder(int id) async {
    _reminderController.sink.add(await DBProvider.db.getReminder(id));
  }

  delete(int id) async {
    await DBProvider.db.deleteReminder(id);
    getReminder();
  }

  update(Reminders r) async {
    await DBProvider.db.updateReminder(r);
    getReminder();
  }

  add(Reminders r) async {
    await DBProvider.db.newReminder(r);
    getReminder();
  }
  getId(String name) async {
    return getId(name);
    getReminder();
  }
}
