import 'dart:async';
import 'dart:core';
import 'package:flutter/cupertino.dart';
import 'package:flutter_procrastinot/core/models/offlineModels/listitems.dart';
import 'package:flutter_procrastinot/core/models/offlineModels/lists.dart';
import 'package:sqflite/sqflite.dart';
import 'database_creator.dart';


class DBProvider {
  DBProvider._();
  static final DBProvider db = DBProvider._();
  static Database _database;

  Future<Database> get database async {
    if (_database != null) {
      return _database;
    }
    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  newLists(Lists l,List<ListItems> li) async {
    final db = await database;
    //get the biggest id in the table
    var table = await db.rawQuery("SELECT MAX(listsid)+1 as listsid FROM Lists");
    int id = table.first["listsid"];
    //insert to the table using the new id
    var raw = await db.rawInsert(
        "INSERT Into Lists(listsid,title,date_modified)"
            " VALUES (?,?,?)",
        [id, l.title,l.date_modified]);
    var tableItems;
    var rawItems;
    int itemsid;
    for(int x = 0; x<li.length;x++)
      {
        tableItems = await db.rawQuery("SELECT MAX(listitemsid)+1 as listitemsid FROM ListItems");
        itemsid = tableItems.first["listitemsid"];
        rawItems = await db.rawInsert(
            "INSERT Into ListItems(listitemsid,body,status,listsid)"
                " VALUES (?,?,?,?)",
            [itemsid, li.elementAt(x).body,li.elementAt(x).status,id]);
      }
    return raw;
  }

  Future<List<ListItems>> getAllListItems() async {
    final db = await database;
    var res = await db.query("ListItems");
    List<ListItems> listitems = res.isNotEmpty ? res.map((c) => ListItems.fromJson(c)).toList() : [];
    return listitems;
  }

  Future<List<Lists>> getAllLists() async {
    final db = await database;
    var res = await db.query("Lists");
    List<Lists> list = res.isNotEmpty ? res.map((c) => Lists.fromJson(c)).toList() : [];
    return list;
  }

  updateLists(Lists l,List<ListItems> li) async {
    final db = await database;
    var res = await db.update("Lists", l.toJson(), where: "listsid = ?", whereArgs: [l.listsid]);
    var tableItems;
    var rawItems;
    for(int x = 0; x<li.length;x++)
    {
      tableItems = await db.update("Lists", li.elementAt(x).toJson(),where:"listitemsid = ?", whereArgs: [l.listsid]);
      int itemsid = tableItems.first["listitemsid"];
      //insert to the table using the new id
      rawItems = await db.rawInsert(
          "INSERT Into ListItems(listitemsid,body,status,listsid)"
              " VALUES (?,?,?,?)",
          [itemsid, l.title,l.date_modified,l.listsid]);
    }
    return res;
  }

  deleteLists(int id) async {
    final db = await database;
    db.delete("Lists", where: "listsid = ?", whereArgs: [id]);
    db.delete("ListItems", where: "listsid = ?", whereArgs: [id]);
  }
}

class ListsBloc {

  ListsBloc() {
    getLists();
  }

  final _listsController =  StreamController<List<Lists>>.broadcast();
  get lists => _listsController.stream;

  dispose() {
    _listsController.close();
  }


  getLists() async
  {
    _listsController.sink.add(await DBProvider.db.getAllLists());
  }

  delete(int id) async {
    await DBProvider.db.deleteLists(id);
    getLists();
  }

  Future<void> update(Lists l,List<ListItems> li) async {
    await DBProvider.db.updateLists(l,li);
    getLists();
  }

  add(Lists l,List<ListItems> li) async {
    await DBProvider.db.newLists(l, li);
    getLists();
  }
}

class ListItemsBloc{
  ListItemsBloc()
  {
    getListItems();
  }
  final _listitemsController =  StreamController<List<ListItems>>.broadcast();
  get listitems => _listitemsController.stream;

  dispose() {
    _listitemsController.close();
  }

  getListItems() async{
      _listitemsController.sink.add(await DBProvider.db.getAllListItems());
  }
}