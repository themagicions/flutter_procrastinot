import 'dart:convert';

import 'package:flutter_procrastinot/constants.dart';

Reminders reminderFromJson(String str) {
  final jsonData = json.decode(str);
  return Reminders.fromJson(jsonData);
}

String reminderToJson(Reminders data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class Reminders {
  int reminderid;
  String name;
  String date;
  String time;
  String date_modified;

  Reminders({
    this.reminderid,
    this.name,
    this.date,
    this.time,
    this.date_modified,
  });

  factory Reminders.fromJson(Map<String, dynamic> json) => new Reminders(
    reminderid: json["reminderid"],
    name: json["name"],
    date: json["date"],
    time: json["time"],
    date_modified: json["date_modified"]
  );

  Map<String, dynamic> toJson() => {
    "reminderid": reminderid,
    "name": name,
    "date": date,
    "time": time,
    "date_modified": date_modified
  };
}