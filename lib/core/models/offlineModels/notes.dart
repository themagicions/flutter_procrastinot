import 'dart:convert';

Notes notesFromJson(String str) {
  final jsonData = json.decode(str);
  return Notes.fromJson(jsonData);
}

String notesToJson(Notes data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class Notes {
  int notesid;
  String text;
  String date_modified;

  Notes({
    this.notesid,
    this.text,
    this.date_modified
  });

  factory Notes.fromJson(Map<String, dynamic> json) => new Notes(
    notesid: json["notesid"],
    text: json["text"],
    date_modified: json["date_modified"]
  );

  Map<String, dynamic> toJson() => {
    "notesid": notesid,
    "text": text,
    "date_modified": date_modified
  };
}