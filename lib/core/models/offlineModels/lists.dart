import 'dart:convert';
class Lists {
 int listsid;
  String title;
  String date_modified;

  Lists({
    this.listsid,
    this.title,
    this.date_modified
  });

 static final columns = ["listsid", "title", "date_modified"];

  factory Lists.fromJson(Map<String, dynamic> json) => new Lists(
      listsid: json["listsid"],
      title: json["title"],
      date_modified: json["date_modified"]
  );

  Map<String, dynamic> toJson() => {
    "listsid": listsid,
    "title": title,
    "date_modified": date_modified
  };
}


Lists listFromJson(String str) {
  final jsonData = json.decode(str);
  return Lists.fromJson(jsonData);
}

String listToJson(Lists data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}