import 'dart:convert';

ListItems listFromJson(String str) {
  final jsonData = json.decode(str);
  return ListItems.fromJson(jsonData);
}

String listToJson(ListItems data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class ListItems {
  int listitemsid;
  String body;
  int status;
  int listsid;

  static final columns = ["listitemsid", "body","status","listsid"];

  ListItems({
    this.listitemsid,
    this.body,
    this.status,
    this.listsid
  });

  factory ListItems.fromJson(Map<String, dynamic> json) => new ListItems(
      listitemsid: json["listitemsid"],
      body: json["body"],
      status: json["status"],
      listsid: json["listsid"]
  );

  Map<String, dynamic> toJson() => {
    "listitemsid": listitemsid,
    "body": body,
    "status": status,
    "listsid": listsid
  };
}